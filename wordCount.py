from operator import add
from pyspark import SparkFiles, SparkContext
import boto3

my_name_bucket = "anotherone8415dori" # to change

if __name__ == "__main__":
    # get the file from the S3
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(my_name_bucket)

    obj = s3.Object(my_name_bucket, "219-0.txt")

    text = obj.get()['Body'].read().decode("utf-8")
    lines = text.split("\n")

    sc = SparkContext()

    text_file = sc.parallelize(lines)

    # count the words
    counts = text_file.flatMap(lambda x: x.split(' '))
    counts = counts.map(lambda x: (x, 1)).reduceByKey(add)
    counts.saveAsTextFile("out")
