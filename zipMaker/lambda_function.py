import json
import csv
import pickle
import math
import boto3
import numpy as np
import sklearn

my_name_bucket = "anotherone8415dori"

# GET_DATA : from a file content, retrieve the matrix of data X and, if the
# results are known (withRes), return the result y which should be the last
# column of the data.
def get_data(file, withRes):
    matrix = np.array([line.split(',') for line in file.split('\n') if line != ""])

    if withRes :
        X = matrix[:, :-1]
        y = matrix[:, -1]

        return X, y
    else:
        return matrix

# ACCURACY : computes the accuracy of the prediction in regards to the real
# results.
def accuracy(pred, real):
    return sum([str(i)==str(j) for i, j in zip(pred,real)]) / len(real)

# LAMBDA_HANDLER : returns the prediction of the test dataset using a SVM
# model. It will also print the accuracy of the prediction of the valid dataset
# and print the prediction of the test dataset.
def lambda_handler(event, context):
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(my_name_bucket)

    pickle_svm= pickle.loads(s3.Object(my_name_bucket, "model_svm.p").get()['Body'].read())

    file_valid = s3.Object(my_name_bucket, "ds1Val.csv").get()['Body'].read().decode('utf-8')
    file_test  = s3.Object(my_name_bucket, "ds1Test.csv").get()['Body'].read().decode('utf-8')

    X_valid, y_valid = get_data(file_valid, True)
    X_test = get_data(file_test, False)


    pred_svm_valid = pickle_svm.predict(X_valid)
    print("Accuracy of svm on valid set : " + str(accuracy(pred_svm_valid, y_valid)))

    pred_svm_test = pickle_svm.predict(X_test)
    print("Prediction of svm on test set : " + str(pred_svm_test))


    return json.dumps(pred_svm_test.tolist())
