import boto3
import json

my_name_bucket = "anotherone8415dori"
my_function = "arn:aws:lambda:us-east-1:739199900657:function:MLFunction"

if __name__ == "__main__":
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(my_name_bucket)

    client = boto3.client('lambda')

    print(client.invoke(FunctionName = my_function, InvocationType = "RequestResponse", Payload='false'))
    print("Lambda function invoked.")
