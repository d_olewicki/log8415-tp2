from sklearn import svm
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.naive_bayes import GaussianNB

import csv
import numpy as np
import pickle
import math

# Open the files of the training dataset and the validation dataset.
with open('data/ds1Train.csv', 'r') as csvfile:
    csvreader = list(csv.reader(csvfile, delimiter=',', quotechar='|'))
    result = np.array(csvreader).astype('int')

    X_train = result[:, :-1]
    y_train = result[:, -1]

with open('data/ds1Val.csv', 'r') as csvfile:
    csvreader = list(csv.reader(csvfile, delimiter=',', quotechar='|'))
    result = np.array(csvreader).astype('int')

    X_valid = result[:, :-1]
    y_valid = result[:, -1]

##### SVM #####
try:
    with open( "model/model_svm.p", "rb") as f:
        model_svm = pickle.load(f)
except (FileNotFoundError) as e:
    model_svm = svm.SVC(kernel='linear', C=0.05).fit(X_train,y_train)
    with open( "model/model_svm.p", "wb" ) as f:
        pickle.dump( model_svm, f)
pred_valid_svm = model_svm.predict(X_valid)

accuracy_svm = sum(pred_valid_svm == y_valid) / len(y_valid)
print( "Accuracy of SVM : " + str(accuracy_svm) )

##### RandomForest #####
try:
    with open( "model/model_rf.p", "rb") as f:
        model_rf = pickle.load(f)
except (FileNotFoundError) as e:
    model_rf = RandomForestClassifier(n_estimators = 1000, random_state = 15).fit(X_train, y_train)
    with open( "model/model_rf.p", "wb" ) as f:
        pickle.dump( model_rf, f)

pred_valid_rf = model_rf.predict(X_valid)
accuracy_rf = sum(pred_valid_rf == y_valid) / len(y_valid)
print( "Accuracy of RandomForest : " + str(accuracy_rf) )

# ##### Nearest Neighbors #####
try:
    with open( "model/model_kn.p", "rb") as f:
        model_kn = pickle.load(f)
except (FileNotFoundError) as e:
    model_kn = KNeighborsClassifier(1).fit(X_train, y_train)
    with open( "model/model_kn.p", "wb" ) as f:
        pickle.dump( model_kn, f)

pred_valid_kn = model_kn.predict(X_valid)
accuracy_kn = sum(pred_valid_kn == y_valid) / len(y_valid)

print( "Accuracy of Nearest Neighbors : " + str(accuracy_kn) )

##### Neural Net #####
try:
    with open( "model/model_nn.p", "rb") as f:
        model_nn = pickle.load(f)
except (FileNotFoundError) as e:
    model_nn = MLPClassifier(alpha=5).fit(X_train, y_train)
    with open( "model/model_nn.p", "wb" ) as f:
        pickle.dump( model_nn, f)

pred_valid_nn = model_nn.predict(X_valid)
accuracy_nn = sum(pred_valid_nn == y_valid) / len(y_valid)

print( "Accuracy of Neural Net : " + str(accuracy_nn) )
