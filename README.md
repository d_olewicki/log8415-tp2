# LOG8415-TP2

# Comments on the content of our scripts and Makefile.

In your bucket, you should have the 219-0.txt, the pickles generated when computing the Machine Learning part of our code and the datasets (train/valid/test) all on the roots of the project.

We provide a makefile that perform the different part of our implementation.

## wordCount.py :
The variable "my_name_bucket" should be updated with your bucket name.
The code can then be launch by using the command "make wordcount".

## MLModels.py :
The Machine Learning part of the project needs to have the training and validation set files in a ./data/ directory.
It can be launch using the command "make ml".

## ./zipMaker/lambda_function.py :
Code of the lambda function. Can be loaded to the Lambda using the command "make lambda" from the root directory. The variable "my_name_bucket" should be updated with your bucket name. This will also launch the invoke from the following file.

## lambda_invoke.py :
Invokes the lambda_function from the lambda. Should update the my_name_bucket and my_function to yours. Can launch the function with the command "make lambda" or "make invoke" if everything is already set up on the Lambda.

##SparkLDA.py :
This Script is written for the answer of the Q4 which will show you the implementation of the Latent Dirichlet Allocation (LDA) algorithm which is one of the MLlib algorithm of Spark.

##TransformationAndActions.py :
This script shows the transformations and actions available in the spark for the Q1.






### How to fork this repository?
Please follow the instructions here:
https://help.github.com/articles/fork-a-repo/

Once you forked thi repository, create a folder with a specific name for your group.
Example: "tigers_2019_tp2"

Before the deadline, when you are ready to submit your work, make a pull request:
https://help.github.com/articles/creating-a-pull-request-from-a-fork/

Questions?
Send me an email: a.abtahizadeh[at]polymtl[dot]ca
