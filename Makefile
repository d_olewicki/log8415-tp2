all: wordcount ml install s3 s3ToLambda invoke


wordcount :
	rm -rf out
	python3.7 wordCount.py

ml:
	mkdir -p model
	python3.7 MLModels.py

install:
	rm -rf lambda_function.zip zipMaker/numpy zipMaker/scipy zipMaker/sklearn
	python3.7 -m pip install scikit-learn --target ./zipMaker/ --system
	cd zipMaker
	zip -q -r lambda_function.zip .
	cd ../

s3:
	aws s3 cp zipMaker/lambda_function.zip s3://anotherone8415dori/

s3ToLambda:
	aws lambda update-function-code --function-name MLFunction --s3-bucket anotherone8415dori --s3-key lambda_function.zip

invoke:
	python3.7 lambda_invoke.py

lambda : install s3 s3ToLambda invoke
