# Notes from the labs to share with the group

# CONFIGURE SPARK AND PYSPARK
sto install :
sudo apt-get install python3.7
                     awscli
                     python3-pip
                     default-jre
                     default-jdk

aws configure
=> enter the data from account

vim ~/.aws/credentials
=> copy account details each time

aws s3 ls s3://anotherone8415dori

wget http://apache.mirror.vexxhost.com/spark/spark-2.4.0/spark-2.4.0-bin-hadoop2.7.tgz
=> gets the tar
tar xvf spark-2.4.0-bin-hadoop2.7.tgz
=> extract tar

vim ~/.bash_profile
=> add :
export SPARK_HOME=~/spark-2.4.0-bin-hadoop2.7
export PATH=$SPARK_HOME/bin:$PATH
export PYSPARK_PYTHON=python3.7

(checkout: https://towardsdatascience.com/how-to-get-started-with-pyspark-1adc142456ec?fbclid=IwAR3KsA3ykr4r1S7eW5Iyz3sOguX96LZaJW8ODe9tpz_nLXEegqD6HSait-s)
source ~/.bash_profile
=> will update the changes we did

pyspark
=> launch spark with python3.7

For the classifiers checkout : https://scikit-learn.org/stable/auto_examples/classification/plot_classifier_comparison.html

My role = MLRole@8415dori



#HELP FOR PACKAGES ON LAMBDA
So I managed to do it via this

install:
   rm -rf lambda_function.zip numpy scipy sklearn
   pip3.7 install scikit-learn --target .
   zip -q -r9 lambda_function.zip .

upload_to_s3:
   aws s3 cp ./lambda_function.zip s3://anotherone8415dori/

upload_from_s3_to_lambda:
   aws lambda update-function-code --function-name MLFunction --s3-bucket anotherone8415dori --s3-key lambda_function.zip

final: install upload_to_s3 upload_from_s3_to_lambda
