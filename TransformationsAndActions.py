
from pyspark.sql import SQLContext
import pyspark



sc = pyspark.SparkContext(appName="LDA_app")
sqlContext = SQLContext(sc)

text = ["Camel", "Tiger", "Panther", "Monkey", "Goose", "Camel", "Moose", "Frog", "Scorpion", "Camel", "Spider", "Spider","Canary", "Eagle", "Elephant" ]
FilteringText = ["Camel", "Tiger", "Spider"]

print("Reduce")
print(sc.parallelize(text).reduce(lambda t1, t2: t1+t2))

print("---------------------------------------------------------------------------------------------------------------")

print("Collect(func)")
print(sc.parallelize(text).map(lambda a: [a, len(a)]).collect())

print("---------------------------------------------------------------------------------------------------------------")

print("Collect")
print(sc.parallelize(text).flatMap(lambda x: [x,x,x]).collect())

print("---------------------------------------------------------------------------------------------------------------")

print("Count")
print(sc.parallelize(text).count())

print("---------------------------------------------------------------------------------------------------------------")

print("First")
print(sc.parallelize(text).first())

print("---------------------------------------------------------------------------------------------------------------")

print("Take")
print(sc.parallelize(text).take(4))

print("---------------------------------------------------------------------------------------------------------------")

print("CountByKey")
print(sc.parallelize(text).map(lambda k: (k,1)).countByKey().items())

print("---------------------------------------------------------------------------------------------------------------")

print("Filter")
print(sc.parallelize(text).filter(lambda x : x not in FilteringText).take(20))

print("---------------------------------------------------------------------------------------------------------------")

print("Distinct")
print(sc.parallelize(text).distinct().take(20))

print("---------------------------------------------------------------------------------------------------------------")

print("intersection")
print(sc.parallelize(text).intersection(sc.parallelize(FilteringText)).take(20))
